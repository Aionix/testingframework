package BuyOldMethodsMonitored;

import Utils.BrowserSettingsBuy;
import org.junit.Test;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Sergey on 19.08.2016.
 */
public class OldMonitoredIncluded0 extends BrowserSettingsBuy {

    String marina = "OldMonitoredIncluded0";

    @Test
    public void BuyOldMonitoredIncluded0() throws InterruptedException {

        $("#SearchForm_Destination").click();
        $("#SearchForm_Destination").setValue(marina);
        $(byText("OldMonitoredIncluded0, Greece, Agios Efstratios")).click();
        $("#home-search-form-search").click();
        $(".fa.fa-plus.mr-booking-radio").click();
        $(".booking_request_pricing.btn.btn-block.btn-lg.btn-primary").click();         //Req booking
        $("#Pricing_BoatType").click();
        $("#Pricing_BoatType>option[value='3']").click();                               //catamaran
        $(".btn.btn-primary.btn-lg.btn-block.pricing-request").click();                 //continue

        //payment page
        //validation of prices
        //
        $("#select2-PaymentForm_country-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $("#select2-PaymentForm_nationality-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $(byXpath("//ul/li[2]/label/span")).click();    //agree license
        $("#submit-button").click();
        $(".identifier>img").isDisplayed();
        $("#code").setValue("secret3");
        $(".submit").click();
        $(".padding-all-10").isDisplayed();
}}

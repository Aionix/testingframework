package lengthXbeam;

import Utils.BrowserSettingsCreate;
import com.codeborne.selenide.Condition;
import org.junit.Test;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Sergey on 28.07.2016.
 */
public class LenBeamDirectIncluded100 extends BrowserSettingsCreate{
    final String marName = "LenBeamDirectIncluded100";
    final String reqPayment = "100";
    String addMarinaButton = "div>a.btn[href='/midend/marina/step1']";
    String catamaranDaily = "28.8";
    String motorDaily = "100";
    String sailDaily = "24.4";

    
    @Test
    public void CreateLenBeamDirectIncluded100() throws InterruptedException {
        $("div a[href*='authenticate']").isDisplayed();
        $("div a[href*='authenticate']").click();
        //choose radio
        $("#LoginForm_username").setValue("aionix13@yandex.ua");
        $("#LoginForm_password").setValue("123");

        $(".btn.btn-primary").click();
        $(".fa.fa-lg.fa-fw.fa-star").isDisplayed();   // myMarinas
        $(".fa.fa-lg.fa-fw.fa-star").click();
        $(addMarinaButton).click();

        //Filling Step1
        $("#Form1Step_contactFirstName").setValue("Art13");
        $("#Form1Step_contactLastName").setValue("Art13");
        $(".selected-flag").click();
        $(".country-name").hover();
        $(".iti-flag.ua").scrollTo().click();
        $("#Form1Step_contactPhone").setValue("638813980");
        $("#Form1Step_marinaName").setValue(marName);
        $("#Form1Step_marinaBerths").setValue("5");
        $("#Form1Step_contactEmail").setValue("aionix123@yandex.ua");

        $("#select2-Form1Step_marinaCountryId-container").click();
        $(".select2-search__field").setValue("United States").sendKeys(Keys.ENTER);
        //$("#select2-Form1Step_marinaRegionId-container").click();
        //$(".select2-results>ul>li:nth-child(1)").isDisplayed();
        //Thread.sleep(1500);
        //$(".select2-results>ul>li:nth-child(1)").click();
        // $(".select2-search__field").setValue("California").sendKeys(Keys.ENTER);
        $("#Form1Step_marinaCity").setValue("Some City");
        $("#Form1Step_marinaAddress").setValue("test address1");
        $("#method5").click();
        $(".btn.btn-success.pull-right.formBottomButton").click();
        //Filling Step2 //CreateClassLength
        $("#btnAddLengthClass[name*='yt1']").click();
        Thread.sleep(500);
        $("#SeasonPrice_length").setValue("4");
        $("#SeasonPrice_beam").setValue("3");
        $("#SeasonPrice_draught").setValue("3");
        $("#SeasonPrice_capacity").setValue("5");
        $("#SeasonPrice_boat_type").click();
        $("#SeasonPrice_boat_type>option[value='3']").click();
        $("#SeasonPrice_price").setValue(catamaranDaily);
        $("#savePrice").click();
        Thread.sleep(500);
        //Motor
        $("#btnAddLengthClass[name*='yt1']").exists();
        $("#btnAddLengthClass[name*='yt1']").click();
        $("#SeasonPrice_price").setValue(motorDaily);
        $("#SeasonPrice_length").setValue("10");
        $("#SeasonPrice_beam").setValue("5");
        $("#SeasonPrice_draught").setValue("5");
        $("#SeasonPrice_capacity").setValue("5");
        $("#SeasonPrice_boat_type").click();
        $("#SeasonPrice_boat_type>option[value='2']").click();
        $("#savePrice").click();
        Thread.sleep(500);
        //Sail
        $("#btnAddLengthClass[name*='yt1']").exists();
        $("#btnAddLengthClass[name*='yt1']").click();
        $("#SeasonPrice_price").setValue(sailDaily);
        $("#SeasonPrice_length").setValue("20");
        $("#SeasonPrice_beam").setValue("10");
        $("#SeasonPrice_draught").setValue("10");
        $("#SeasonPrice_capacity").setValue("5");
        $("#SeasonPrice_boat_type").click();
        $("#SeasonPrice_boat_type>option[value='1']").click(); //catamaran
        $("#savePrice").click();
        /////////////

        $("#Marina_RequiredPayment").doubleClick();
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        Thread.sleep(1000);
        $("#Marina_RequiredPayment").setValue(reqPayment);
        Thread.sleep(1000);
        $("#requiredPaymentMarinaFeeKind[value='1']").click();  //included

        $(".btn.btn-success.pull-right.formBottomButton.btnSave.terms").click();
        $(".modal-body").isDisplayed();
        $("#license").hover();
        $("#license").click();
        $("#terms").hover();
        $("#terms").click();
        Thread.sleep(1000);
        $("#saveStep").hover();
        $("#saveStep").click();
        Thread.sleep(1000);
        //
        $(".text-center").shouldHave(Condition.exactText("Your marina will get online and bookable in just 3 steps!"));
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").hover().shouldBe(Condition.visible);
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").click();
        // WaitForCondition()
        Thread.sleep(4000);
        $(".text-center").shouldHave(Condition.exactText("Drag an item to the calendar"));
}}

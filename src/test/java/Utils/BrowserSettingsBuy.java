package Utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Sergey on 28.07.2016.
 */
public abstract class BrowserSettingsBuy {
    String aionix123 = "aionix123@yandex.ua";
    String artyom4ik = "artyom4ik@mail.ru";
    String aionix = "aionix@yandex.ua";

    @Before
    public void chromeStarter() {
        Configuration.timeout = 15000;
        System.setProperty("webdriver.chrome.driver","C:\\drivers\\chromedriver.exe");
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--start-maximized");
        WebDriver chromeDriver = new ChromeDriver(option);
        WebDriverRunner.setWebDriver(chromeDriver);

        open("http://elearn.hopto.org:8070/");
        $("#sign_in").click();
        $("#LoginForm_username").setValue(aionix123);
        $("#LoginForm_password").setValue("123");
        $("#myButton1").click();
    }
    @After
    public void close(){
        WebDriverRunner.closeWebDriver();
    }


}

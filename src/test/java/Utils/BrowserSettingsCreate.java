package Utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Sergey on 28.07.2016.
 */
public abstract class BrowserSettingsCreate {
    String addMarina = "div>a.btn[href='/midend/marina/step1']";
    String included = "#requiredPaymentMarinaFeeKind[value='1']";
    String monitored = "#Form1Step_bookingType[value='1']";

    @Before
    public void chromeStarter() {

        Configuration.timeout = 15000;
       // Configuration.holdBrowserOpen = true;
        System.setProperty("webdriver.chrome.driver","C:\\drivers\\chromedriver.exe");
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--start-maximized");

        WebDriver chromeDriver = new ChromeDriver(option);
        WebDriverRunner.setWebDriver(chromeDriver);
       // open("http://elearn.hopto.org:8080/midend/user/register");
        open("http://elearn.hopto.org:8070/midend/user/register");
        $("div a[href*='authenticate']").isDisplayed();
        $("div a[href*='authenticate']").click();
        //choose radio
        $("#LoginForm_username").setValue("aionix14@yandex.ua");
        $("#LoginForm_password").setValue("123");

    }
    @After
    public void close(){
        WebDriverRunner.closeWebDriver();
    }


}

package BuyOldMethods;

import Utils.BrowserSettingsBuy;
import org.junit.Test;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Sergey on 19.08.2016.
 */
public class OldDirectAdded50 extends BrowserSettingsBuy {
    String marina = "OldDirectAdded50";

    @Test
    public void BuyOldDirectAdded100() throws InterruptedException {

        $("#SearchForm_Destination").click();
        $("#SearchForm_Destination").setValue(marina);
        $(byText("OldDirectAdded50, Greece, Agios Efstratios")).click();
        $("#home-search-form-search").click();

        $(".fa.fa-plus.mr-booking-radio").click();
        $("#book").click();                 //continue

        //payment page
        //validation of prices
        //
        $("#select2-PaymentForm_country-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $("#select2-PaymentForm_nationality-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $(byXpath("//ul/li[2]/label/span")).click();    //agree license
        $("#submit-button").click();
        $(".identifier>img").isDisplayed();
        $("#code").setValue("secret3");
        $(".submit").click();
        $(".padding-all-10").isDisplayed();

}}

package smartacad.ua;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Артем on 13.03.2016.
 */
public class SmokeTestCreateMarina {

    @Before
    public void setUp() {
        Configuration.timeout = 10000;

        System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe");
        WebDriver dr = new ChromeDriver();
        // WebDriver dr = new RemoteWebDriver("http://localhost:9515");
        WebDriverRunner.setWebDriver(dr);
    }

    @Test
    public void CreateMarina() {

        open("http://elearn.hopto.org:8080/");
        $("div a[href*='authenticate']").isDisplayed();
        $("div a[href*='authenticate']").click();

        $("#LoginForm_username").setValue("aionix1@yandex.ua");
        $("#LoginForm_password").setValue("123");

        $(".btn.btn-primary").click();
        $(".fa.fa-lg.fa-fw.fa-star").click();                 // myMarinas
        $(".btn.btn-success.col-md-6.pull-left").click();       //add marina
     //   $("td [href*='step1?marinaId=2665']").click();        //выбор марины по ИД

        //Filling Step1
        $("#Form1Step_contactFirstName").setValue("Artyom");
        $("#Form1Step_contactLastName").setValue("Artyom");
        $("#Form1Step_contactPhone").setValue("12345678");
        $("#Form1Step_marinaName").setValue("ArtyomDirect2");   //Marina name
        $("#Form1Step_marinaBerths").setValue("2");
        $("#Form1Step_contactEmail").setValue("aionix2@yandex.ua");
        $("#select2-Form1Step_marinaCountryId-container").click();
        $(".select2-search__field").setValue("United States").sendKeys(Keys.ENTER);

        $("#select2-Form1Step_marinaCountryId-container").click();
        $(".select2-search__field").setValue("United States").sendKeys(Keys.ENTER);
        $("#select2-Form1Step_marinaRegionId-container").click();
        $(".select2-search__field").setValue("California").sendKeys(Keys.ENTER);

        $("#Form1Step_marinaCity").setValue("Some City");
        $("#Form1Step_marinaAddress").setValue("test address1");
        //intervals step1
        $("#Form1Step_marinaInterval").setValue("2").sendKeys(Keys.ENTER);
        $("#Form1Step_marinaMinLength").findElementByCssSelector("div option[value='7']").click();
        $("#Form1Step_marinaMaxLength").findElementByCssSelector("div option[value='15']").click();
        $(".btn.btn-success.pull-right.formBottomButton").click();

        $("html/body/div[1]/div/div[2]/form/div[7]/div/div/div[2]/div/div[7]/div/div[1]/input").click();
        $("html/body/div[1]/div/div[2]/form/div[7]/div/div/div[2]/div/div[7]/div/div[3]/input").click();

        //Step2   заимплементить рандомный выбор фасилитис
        $(".marinacon-yacht-renting").click();
        $(".marinacon-ship-market").click();
        $(".label.label-primary.facilityItem").click();

        $("#Marina_RequiredPayment").setValue("100").click();                    //req Payment

        $(".form-control.form2StepBeam").doubleClick().setValue("2");
        $(".form-control.form2StepDraught").doubleClick().setValue("8");
        $(".form-control.form2Capacity").doubleClick().setValue("10");
        $("div button[value*='1']").click();                                    //update
        $(".logo>a>img").exists();
        $("a[href*='/calendar?']").click();                                     //календарь
        Configuration.holdBrowserOpen = true;

        //Calendar
        $("div [ data-place='780']").dragAndDropTo(".fc-day.fc-widget-content.fc-tue.fc-today.fc-state-highlight");
        $("#mr_place_price").setValue("100");
        $("#mr_place_no").setValue("10");
        $(".btn.btn-primary.mr_place_updater").click();

        //Calendar

        // $(".fc-day.fc-widget-content.fc-thu.fc-today.fc-state-highlight"); // use for drag



        Configuration.holdBrowserOpen = true;
}}

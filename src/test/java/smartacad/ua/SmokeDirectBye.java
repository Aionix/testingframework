package smartacad.ua;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Артем on 10.04.2016.
 */
public class SmokeDirectBye {
    @Before public void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\drivers\\chromedriver.exe");
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--start-maximized");

        WebDriver chromeDriver = new ChromeDriver(option);
        WebDriverRunner.setWebDriver(chromeDriver);
        open("http://www.waoo.dk/");
    }

    @Test

        public void SearchForMarinaAndBye() {
        $("input:not(#search-input)").setValue("test");
        $(byText("Testrupvej 136 9620 Aalestrup")).click();
        $(By.xpath("//button[text()='Tjek adresse']")).click();

        Configuration.holdBrowserOpen = true;
    }
}

package smartacad.ua;

import com.codeborne.selenide.Configuration;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by Sergey on 25.03.2016.
 */
public class TempTests {

    //  public static void main (String args[]) {
 /*       int i = 0;
        for (; i < 2; i=i+5) {
            if (i < 5) continue;
            System.out.println (i);
        }System.out.println (i);

    }*/
    @Test
    public void runner() {
        open("https://todomvc4tasj.herokuapp.com/");

        $("#new-todo").setValue("the first task").pressEnter();
        $("#new-todo").setValue("the second task").pressEnter();
        $("#new-todo").setValue("the third task").pressEnter();
        $("#new-todo").setValue("the fourth task").pressEnter();
        $$("#todo-list li").shouldHave(size(4));
        $$("#todo-list label").shouldHave(exactTexts("the first task", "the second task", "the third task", "the fourth task"));
        $("#todo-list>li:nth-of-type(2) .destroy").click();
        $$("#todo-list li").shouldHave(size(3));
        $("#todo-list>li:nth-of-type(4) input[type='checkbox']").click();
        $("#clear-completed").click();
        $$("#todo-list li").shouldHave(size(2));
        $("#toggle-all").click();
        $("#clear-completed").click();
        $$("#todo-list li").shouldHave(size(0));

    }
}




package D50Square;

import Utils.BrowserSettingsCreate;
import com.codeborne.selenide.Condition;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;

/**
 * Created by Sergey on 01.08.2016.
 */
public class D50SquareIncluded50 extends BrowserSettingsCreate {
    final String marName = "D50SquareIncluded50";
    final String reqPayment = "50";
    String addMarinaButton = "div>a.btn[href='/midend/marina/step1']";
    String catamaranDaily = "10";
    String motorDaily = "100";
    String sailDaily = "24.4";
    String monitored = "#Form1Step_bookingType[value='1']";
    String discount = "50";


    @Test
    public void CreateD50SquareIncluded50() throws InterruptedException {
        $("div a[href*='authenticate']").isDisplayed();
        $("div a[href*='authenticate']").click();
        //choose radio
        $("#LoginForm_username").setValue("aionix12@yandex.ua");
        $("#LoginForm_password").setValue("123");

        $(".btn.btn-primary").click();
        $(".fa.fa-lg.fa-fw.fa-star").isDisplayed();   // myMarinas
        $(".fa.fa-lg.fa-fw.fa-star").click();
        $(addMarinaButton).click();
        $(monitored).click();
        Thread.sleep(1000);
        switchTo().alert().accept();

        //Filling Step1
        $("#Form1Step_contactFirstName").setValue("Art13");
        $("#Form1Step_contactLastName").setValue("Art13");
        $(".selected-flag").click();
        $(".country-name").hover();
        $(".iti-flag.ua").scrollTo().click();
        $("#Form1Step_contactPhone").setValue("638813980");
        $("#Form1Step_marinaName").setValue(marName);
        $("#Form1Step_marinaBerths").setValue("5");
        $("#Form1Step_contactEmail").setValue("aionix123@yandex.ua");

        $("#select2-Form1Step_marinaCountryId-container").click();
        $(".select2-search__field").setValue("United States").sendKeys(Keys.ENTER);
        $("#Form1Step_marinaCity").setValue("Some City");
        $("#Form1Step_marinaAddress").setValue("test address1");
        $("#method2").click();
        //lengths
        Thread.sleep(2000);
        $("#Form1Step_marinaMinLength").isDisplayed();
        $("#Form1Step_marinaMinLength").click();
        $("#Form1Step_marinaMinLength").findElementByCssSelector("div option[value='3']").click();
        $("#Form1Step_marinaMaxLength").click();
        $("#Form1Step_marinaMaxLength").findElementByCssSelector("div option[value='20']").click();
        $(".btn.btn-success.pull-right.formBottomButton").click();

        //Filling Step2 //CreateClassLength
        //catamaran
        $(By.xpath("//input[@name='yt3']")).click();
        $("#type_of_boat>option[value='3']").click();
        $("#daily").setValue(catamaranDaily);
        $("#sqm-confirm").click();

        /////////////
        //Discount
        $(By.xpath("//*[@id='btnAddLengthClass'][@name='yt1']")).click();
        $(".discount-input.form-control[name*='percentage']").setValue(discount);
        $(".discount-input.form-control[name*='days']").setValue("1");
        $(".discount-input.form-control[name*='days']").sendKeys(Keys.ESCAPE);
        switchTo().alert().accept();

        $("#Marina_RequiredPayment").doubleClick();
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        Thread.sleep(1000);
        $("#Marina_RequiredPayment").setValue(reqPayment);
        Thread.sleep(1000);
        $("#requiredPaymentMarinaFeeKind[value='1']").click();  //included

        $(".btn.btn-success.pull-right.formBottomButton.btnSave.terms").click();
        $(".modal-body").isDisplayed();
        $("#license").hover();
        $("#license").click();
        $("#terms").hover();
        $("#terms").click();
        Thread.sleep(1000);
        $("#saveStep").hover();
        $("#saveStep").click();
        Thread.sleep(1000);
        //
        $(".text-center").shouldHave(Condition.exactText("Your marina will get online and bookable in just 3 steps!"));
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").hover().shouldBe(Condition.visible);
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").click();
        // WaitForCondition()
        Thread.sleep(4000);
        $(".text-center").shouldHave(Condition.exactText("Drag an item to the calendar"));
    }
}
package BuyUpTo;

import Utils.BrowserSettingsBuy;
import org.junit.Test;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Sergey on 08.08.2016.
 */
public class BuyUptoDirectAdded50 extends BrowserSettingsBuy {
    String marina = "UptoDirectAdded50";
    String length = "3";
    String beam = "3";
    String draught = "3";

    @Test
            public void BuyUptoDirectAdded50() throws InterruptedException {

        $("#SearchForm_Destination").click();
        $("#SearchForm_Destination").setValue(marina);
        $(byText("UpToDirectAdded50, United States, Florida")).click();
        $("#home-search-form-search").click();
        $("#Length").setValue(length);
        $("#Beam").setValue(beam);
        $("#Draught").setValue(draught);
        $("#BoatType>option[value='3']").click();   //catamaran
        $("#nbb-searchButton").click();
        $(".fa.fa-plus.mr-booking-radio").click();
        $("#nbb-btn-real").click();                 //continue

        //payment page
        //validation of prices
        //
        $("#select2-PaymentForm_country-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $("#select2-PaymentForm_nationality-container").click();
        $(".select2-search__field").setValue("uk").pressEnter();
        $(byXpath("//ul/li[2]/label/span")).click();    //agree license
        $("#submit-button").click();
        $(".identifier>img").isDisplayed();
        $("#code").setValue("secret3");
        $(".submit").click();
        $(".padding-all-10").isDisplayed();


    }
}


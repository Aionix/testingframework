package customMarinaCreation;

import Utils.BrowserSettingsCreate;
import com.codeborne.selenide.Condition;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Sergey on 29.07.2016.
 */
public class CustomDirectAdded100 extends BrowserSettingsCreate{
    final String marName = "CustomDirectAdded100";
    final String reqPayment = "100";
    String addMarinaButton = "div>a.btn[href='/midend/marina/step1']";
    String catamaranDaily = "28.8";
    String motorDaily = "100";
    String sailDaily = "24.4";


    @Test
    public void CreateCustomDirectAdded100() throws InterruptedException {

        $(".btn.btn-primary").click();
        $(".fa.fa-lg.fa-fw.fa-star").isDisplayed();   // myMarinas
        $(".fa.fa-lg.fa-fw.fa-star").click();
        $(addMarinaButton).click();

        //Filling Step1
        $("#Form1Step_contactFirstName").setValue("Art13");
        $("#Form1Step_contactLastName").setValue("Art13");
        $(".selected-flag").click();
        $(".country-name").hover();
        $(".iti-flag.ua").scrollTo().click();
        $("#Form1Step_contactPhone").setValue("638813980");
        $("#Form1Step_marinaName").setValue(marName);
        $("#Form1Step_marinaBerths").setValue("5");
        $("#Form1Step_contactEmail").setValue("aionix123@yandex.ua");

        $("#select2-Form1Step_marinaCountryId-container").click();
        $(".select2-search__field").setValue("United States").sendKeys(Keys.ENTER);
        $("#select2-Form1Step_marinaRegionId-container").click();
        $(".select2-search__field").setValue("California").sendKeys(Keys.ENTER);
        $("#Form1Step_marinaCity").setValue("Some City");
        $("#Form1Step_marinaAddress").setValue("test address1");
        $("#method4").click();                                                          //method
        $("#Form1Step_marinaInterval").click();
        $("#Form1Step_marinaInterval>option[value='custom']").click();
        //Intervals 3-4
        $("#BerthLength_min_lenght").click();
        $("#BerthLength_min_lenght>option[value='3']").click();
        $("#BerthLength_max_lenght").click();
        $("#BerthLength_max_lenght>option[value='4']").click();
        $(add).click();
        //Intervals 8-10
        $("#BerthLength_min_lenght").click();
        $("#BerthLength_min_lenght>option[value='8']").click();
        $("#BerthLength_max_lenght").click();
        $("#BerthLength_max_lenght>option[value='10']").click();
        $(add).click();
        //Intervals 15-20
        $("#BerthLength_min_lenght").click();
        $("#BerthLength_min_lenght>option[value='15']").click();
        $("#BerthLength_max_lenght").click();
        $("#BerthLength_max_lenght>option[value='20']").click();
        $(add).click();
        $("button.btn.btn-default:not(#closeRegionModal)").click();                        //close
        $(".modal-dialog.ui-draggable").is(Condition.disappear);
        $(".btn.btn-success.pull-right.formBottomButton").click();
        //catamaran
        $("button#btnAddLengthClass").click();
        $("#BerthlengthSeason_length_id").click();
        $("//option[text()='3.00-4.00(m)']").shouldBe(Condition.appear);
        $("//option[text()='3.00-4.00(m)']").click();

        $("#BerthlengthSeason_beam").setValue("3");
        $("#BerthlengthSeason_draught").setValue("3");
        $("#BerthlengthSeason_boat_type").click();
        $("#BerthlengthSeason_boat_type>option[value='3']").click();
        $("#BerthlengthSeason_places").setValue("5");
        $("#prices_daily").setValue(catamaranDaily);
        $("input.btn.btn-primary").click();
        //motor
        $("button#btnAddLengthClass").click();
        $("#BerthlengthSeason_length_id").click();
        $("//option[text()='8.00-10.00(m)']").click();
        $("#BerthlengthSeason_beam").setValue("5");
        $("#BerthlengthSeason_draught").setValue("5");
        $("#BerthlengthSeason_boat_type").click();
        $("#BerthlengthSeason_boat_type>option[value='2']").click();
        $("#BerthlengthSeason_places").setValue("5");
        $("#prices_daily").setValue(motorDaily);
        $("input.btn.btn-primary").click();
        //sail
        $("button#btnAddLengthClass").click();
        $("#BerthlengthSeason_length_id").click();
        $("//option[text()='15.00-20.00(m)']").click();
        $("#BerthlengthSeason_beam").setValue("5");
        $("#BerthlengthSeason_draught").setValue("5");
        $("#BerthlengthSeason_boat_type").click();
        $("#BerthlengthSeason_boat_type>option[value='2']").click();
        $("#BerthlengthSeason_places").setValue("5");
        $("#prices_daily").setValue(sailDaily);
        $("input.btn.btn-primary").click();
        ////////////////////////////
        //$("#requiredPaymentMarinaFeeKind[value='1']").click();  //included
        $("#Marina_RequiredPayment").doubleClick();
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        $("#Marina_RequiredPayment").sendKeys(Keys.BACK_SPACE);
        Thread.sleep(1000);
        $("#Marina_RequiredPayment").setValue(reqPayment);
        Thread.sleep(1000);
        $(".btn.btn-success.pull-right.formBottomButton.btnSave.terms").click();
        $(".modal-body").isDisplayed();
        $("#license").hover();
        $("#license").click();
        $("#terms").hover();
        $("#terms").click();
        Thread.sleep(1000);
        $("#saveStep").hover();
        $("#saveStep").click();
        Thread.sleep(1000);
        //
        $(".text-center").shouldBe(Condition.exactText("Your marina will get online and bookable in just 3 steps!"));
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").hover().shouldBe(Condition.visible);
        $(".btn.btn-success.pull-right.formBottomButton[value='Save']").click();
        // WaitForCondition()

        $(".text-center").shouldHave(Condition.exactText("Drag an item to the calendar"));
        $(".text-center").shouldBe(Condition.exist);                                                    //refactor waiting



    }
WebElement add = $(By.xpath(".//*[@class='btn btn-primary'][@value='Add']"));
}

